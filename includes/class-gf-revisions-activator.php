<?php

/**
 * Fired during plugin activation
 *
 * @link       https://kozan.dev
 * @since      1.0.0
 *
 * @package    Gf_Revisions
 * @subpackage Gf_Revisions/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Gf_Revisions
 * @subpackage Gf_Revisions/includes
 * @author     Jared Kozak <jared@kozan.dev>
 */
class Gf_Revisions_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
