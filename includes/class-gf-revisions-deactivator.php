<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://kozan.dev
 * @since      1.0.0
 *
 * @package    Gf_Revisions
 * @subpackage Gf_Revisions/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Gf_Revisions
 * @subpackage Gf_Revisions/includes
 * @author     Jared Kozak <jared@kozan.dev>
 */
class Gf_Revisions_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
