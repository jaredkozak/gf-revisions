=== Plugin Name ===
Contributors:
Donate link: https://kozan.dev
Tags: gravity, forms, revision
Requires at least: 3.0.1
Tested up to: 5.8.2
Stable tag: 5.8.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Add a revisions widget to track and add new entry revisions for a specific wordpress form.

== Description ==

Use this plugin to add a revisions widget to your wordpress website.

For example:

`[gf_revisions form_id="1" max_revisions=3 key_by="4" value_by="1" form_url="/form" no_entry_text="You have not submitted a revision. Submit one by clicking the button below."]`

Revisions will default to the currently authenticated user. If the user has administrative capabilities
they will be able to access other users revisions by adding "user_id=X" to the URL with the revision hosted on it. 

== Installation ==

This section describes how to install the plugin and get it working.

1. Upload `gf-revisions.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about foo bar? =

Answer to foo bar dilemma.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

= 1.0 =
* Initial version
