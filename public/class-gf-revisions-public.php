<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://kozan.dev
 * @since      1.0.0
 *
 * @package    Gf_Revisions
 * @subpackage Gf_Revisions/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Gf_Revisions
 * @subpackage Gf_Revisions/public
 * @author     Jared Kozak <jared@kozan.dev>
 */
class Gf_Revisions_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gf_Revisions_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gf_Revisions_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gf-revisions-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gf_Revisions_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gf_Revisions_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gf-revisions-public.js', array( 'jquery' ), $this->version, false );

	}

	public function init_shortcodes() {
		// die('hey');
		add_shortcode( "gf_revisions", array($this, "shortcode_gf_revisions" ));
		add_filter( 'gform_pre_render', array($this, "populate_revision") );
	}
	public function populate_revision($form) {

		// if ($_GET['read_only'] === 'true') {

		// 	return '';
		// }

		$revision_from = $_GET['revision_from'];
		if (is_null($revision_from)) {
			return $form;
		}
		if (!is_user_logged_in()) {
			return $form;
		}
		$current_user = wp_get_current_user();
		try {
			$entry = GFAPI::get_entry( $revision_from );
		} catch (WP_Error $e) {
			return $form;
		}

		if (is_null($entry) || is_wp_error( $entry )) {
			return $form;
		}
		// var_dump($form);
		// echo "<br /><br />";
		// print_r(json_encode($entry));
		// die($form["id"]);
		if (intval($entry["form_id"]) !== $form["id"]) {
			return $form;
		}

		if (is_null($entry["created_by"])) {
			return $form;
		}

		// print_r(json_encode($entry["created_by"]));
		// print_r(json_encode($current_user->ID));
		if (intval($entry["created_by"]) !== $current_user->ID) {
			return $form;
		}

		foreach( $form["fields"] as &$field ) {
			// var_dump($field);
			// print_r(json_encode($field));
			if (!is_null($field["inputs"])) {
				// var_dump($field["inputs"]);

				foreach( $field->inputs as $key => &$input ) {
					// $input[$key]["WHAT"] = "NOOOOOOOOOOOOOOOO";


					if (!is_null($entry[$input["id"]])) {
						// var_dump($input["id"]);
						// var_dump($entry[$input["id"]]);
						// $input["defaultValue"] = "AAAAH";
						$input["defaultValue"] = $entry[$input["id"]];
					}
				}
				// print_r(json_encode($field["inputs"]));
				// echo "<br />";
				// var_dump($field);
				// print_r(json_encode($field));

				if (!is_null($entry[strval($field["id"])])) {
					// var_dump($entry[strval($field["id"])]);
					$field["defaultValue"] = $entry[strval($field["id"])];
				}

				if ($_GET['read_only'] === 'true') {
					// var_dump($field);
					$field['displayOnly'] = 'true';
				}
				
			}
			// if ( $field->id == 8 ) {
				// $field->placeholder = 'This is my placeholder';
			// }
		}


		return $form;
	}

	public function shortcode_gf_revisions($atts) {
		$atts = shortcode_atts( array(
			'require_authentication' => 'true',
			'max_revisions' => "3",
			'no_entry_text' => "No revisions found",
			'key_by' => null,
			'value_by' => null,
			'form_id' => null,
			'form_url' => null
		), $atts, 'gf_revisions' );
		// var_dump($atts);
		// die();

		if ( is_null($atts['form_id'])) {
			return "must provide form_id";
		}
		if ( is_null($atts['form_url'])) {
			return "must provide form_url";
		}

		if ( is_null($atts['key_by'])) {
			return "must provide key_by";
		}

		if ( is_null($atts['value_by'])) {
			return "must provide value_by";
		}

		if ($atts['require_authentication'] === 'true' && !is_user_logged_in()) {
			return "";
		}

		$current_user = wp_get_current_user();
		if (current_user_can('administrator') && array_key_exists("user_id", $_GET)) {
			$current_user = get_user_by('id', $_GET['user_id']);
			if (!$current_user) {
				return "Invalid ID";
			}
		}

		$form = GFAPI::get_form( $atts['form_id'] );
		if (is_wp_error( $form )) {
			return "invalid form";
		}

		// TODO: if admin, check if user_id is set in the query.

		$search_criteria = array(
    
			'status'     => 'active', //Active forms 
			'field_filters' => array( //which fields to search
				array(
					'key' => 'created_by',
					'value' => $current_user->ID, //Current logged in user
				)
			  )
		);
		$sorting = array( 'key' => 'date_created', 'direction' => 'ASC' );

		$entries = GFAPI::get_entries( $atts['form_id'], $search_criteria, $sorting);
		// var_dump($entries);

		$revisionMatrix = array();

		$maxRevisionCount = intval($atts["max_revisions"]);

		// var_dump($form);
		$arr_key_by = explode(",",$atts['key_by']);
		foreach ($entries as $entry) {
			// DETERMINE KEY
			$key = "";
			foreach ($arr_key_by as $i_key_by => $key_by) {
				foreach( $form["fields"] as &$field ) {
					// var_dump($field["id"]);
					// var_dump(intval($atts['key_by']));
					if ($field["id"] != intval($key_by)) {
						continue;
					}

					if (is_null($field["inputs"]) || $field["inputs"] == "") {
						$key .= $entry[$field["id"]];

					} else {
						foreach( $field->inputs as &$input ) {
							if (!is_null($entry[$input["id"]])) {
								// var_dump($input["id"]);
								// var_dump($entry[$input["id"]]);
								// $input["defaultValue"] = "AAAAH";
								// $input["defaultValue"] = $entry[$input["id"]];
								$val = strval($entry[$input["id"]]);
								// var_dump($key);
	
								$key .= $val . " ";
								// var_dump($key);
								// echo "<br />";
							}
						}
						// print_r(json_encode($field["inputs"]));
						// echo "<br />";
						// var_dump($field);
						// print_r(json_encode($field));
					}
				}

				if ($i_key_by + 1 < count($arr_key_by)) {
					$key .= ' / ';
				}
				// var_dump($entry[$field["id"]]);
				// echo "<br /> <br />";
				// var_dump($key);
			}

			var_dump($key);
			// KEY DETERMINED

			if (!$revisionMatrix[$key]) {
				$revisionMatrix[$key] = array();
			}

			$index = count($revisionMatrix[$key]);
			if ($index + 1 >= $maxRevisionCount) {
				$maxRevisionCount = $index + 1;
			}

			$revisionMatrix[$key][$index] = $entry;
		};

		// var_dump($revisionMatrix);
		// print_r(json_encode($revisionMatrix));

		if (count($revisionMatrix) == 0) {
			return '<p class="no-revisions-found">' . $atts["no_entry_text"] . "</p>";
		}

		$table = '<table class="revisions">';
		$table .= "<thead>";
		$table .= "<tr>";
		$table .= "<th></th>";
		
		for ($i = 0; $i < $maxRevisionCount; $i++) {
			if ($i === 0) {
				$table .= "<th>Original</th>";
			} else {
				$table .= "<th>Revision " . strval($i) . "</th>";
			}
		} 
		$table .= "</tr>";
		$table .= "</thead>";
		$table .= "<tbody>";

		foreach ($revisionMatrix as $revisionKey => $revisions) {

			$table .= "<tr>";

			$table .= "<td>" . $revisionKey ."</td>";
			foreach ($revisions as $revisionIndex => $revision) {

				// $table .= "<td>" . $revision["id"] ."</td>";
				$table .= "<td>" . $revision[$atts["value_by"]] ."</td>";
			}

			$revision_from = "";
			if (count($revisions) > 0) {
				$last_revision = $revisions[count($revisions) - 1];
				$revision_from = '&revision_from=' . $last_revision["id"];
			}

			if (count($revisions) < $atts["max_revisions"]) {
				$table .= '<td>';

				$table .= '<a href="' . $atts["form_url"] . "?revision=" . (count($revisions)) . $revision_from . '" class="new-revision-btn">New Revision</a>';
				$table .= '</td>';
			}

			for ($i = count($revisions) + 1; $i < $maxRevisionCount; $i++) {

				$table .= "<td></td>";
			}

			// for ($i = count())

			$table .= "</tr>";
		}

		$table .= "</tbody>";

		$table .= "</table>";

		return $table;

	}

}
