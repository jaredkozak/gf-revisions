<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://kozan.dev
 * @since             1.0.0
 * @package           Gf_Revisions
 *
 * @wordpress-plugin
 * Plugin Name:       Gravity Forms Revisions
 * Plugin URI:        https://gitlab.com/jaredkozak/gf-revisions
 * Description:       Add a revisions widget to track and add new entry revisions for a specific wordpress form.
 * Version:           1.0.0
 * Author:            Jared Kozak
 * Author URI:        https://kozan.dev
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       gf-revisions
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'GF_REVISIONS_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-gf-revisions-activator.php
 */
function activate_gf_revisions() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gf-revisions-activator.php';
	Gf_Revisions_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-gf-revisions-deactivator.php
 */
function deactivate_gf_revisions() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gf-revisions-deactivator.php';
	Gf_Revisions_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_gf_revisions' );
register_deactivation_hook( __FILE__, 'deactivate_gf_revisions' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-gf-revisions.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_gf_revisions() {

	$plugin = new Gf_Revisions();
	$plugin->run();

}
run_gf_revisions();
